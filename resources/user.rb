actions :create, :remove
default_action :create

attribute :username, :kind_of => String, :name_attribute => true, :required => true
attribute :firstname, :kind_of => String
attribute :lastname, :kind_of => String
attribute :fullname, :kind_of => String
attribute :displayname, :kind_of => String
attribute :homedir, :kind_of => String
attribute :gecos, :kind_of => String
attribute :email, :kind_of => String
attribute :shell, :kind_of => String, :default => '/bin/bash'
attribute :password, :kind_of => String
attribute :uidnumber, :kind_of => Integer
attribute :gidnumber, :kind_of => Integer
attribute :usergroup, :kind_of => [TrueClass, FalseClass], :default => true

attribute :sshpubkey, :kind_of => Array

attribute :street, :kind_of => String
attribute :city, :kind_of => String
attribute :state, :kind_of => String
attribute :postal_code, :kind_of => String
attribute :phone, :kind_of => String
attribute :mobile, :kind_of => String
attribute :pager, :kind_of => String
attribute :fax, :kind_of => String
attribute :carlicense, :kind_of => String
attribute :orgunit, :kind_of => String
attribute :title, :kind_of => String
attribute :manager, :kind_of => String
attribute :department_number, :kind_of => String
attribute :employee_number, :kind_of => String
attribute :employee_type, :kind_of => String
attribute :preferred_langugae, :kind_of => String

attr_accessor :exists #This is a standard ruby accessor, use this to set flags for current state.

