actions :create, :remove
default_action :create

attribute :name, kind_of: String
attribute :desc, kind_of: String
attribute :gidnumber, kind_of: String

attribute :nonposix, kind_of: [TrueClass, FalseClass], default: false
attribute :external, kind_of: [TrueClass, FalseClass], default: false

