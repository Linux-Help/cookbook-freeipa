def whyrun_supported?
  true
end

use_inline_resources

action :remove do
  Chef::Log.warn('Remove ipa_group triggered')
end

action :create do
  Chef::Log.warn('Add ipa_group triggered')
end

private

