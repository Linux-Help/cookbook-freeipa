def whyrun_supported?
  true
end

use_inline_resources

action :remove do
  Chef::Log.warn('Remove ipa_user triggered')
end

action :create do
  Chef::Log.debug('Add ipa_user triggered')
  if ipa_krblogin then
    if ipa_userexist?(new_resource.name) then
      Chef::Log.info("User " + new_resource.name + " already exists in IPA")
      new_resource.updated_by_last_action(false)
    else
      Chef::Log.info("User " + new_resource.name + " being added to IPA")
      new_resource.updated_by_last_action(true)
    end
  end
end

private

def ipa_krblogin
  #File.open("/tmp/ipa.admin", "w") do |f|
  #  f.write(passwords["ipa_user_pwd"]
  #end

  if ::File.exist?('/etc/ipa/admin.password') then
    system 'kinit admin -l 1h < /etc/ipa/admin.password &>/dev/null'
    if $? == 0 then
      Chef::Log.info('IPA login successful')
      true
    else
      Chef::Log.fatal('IPA login failed')
      false
    end
  else
    Chef::Log.fatal('IPA Admin Password file does not exist')
    false
  end
end

def ipa_userexist?(username)
  check = `/usr/bin/ipa user-find --pkey-only --raw | /usr/bin/tr -d " " | /bin/grep "^uid:" | /bin/cut -b 5-`.split
  if check.include?(username) then
    true
  else
    false
  end
end

def ipaFirstName(name)
  name.gsub(/\s+/m, ' ').strip.split(" ")[0]
end

def ipaLastName(name)
  name.gsub(/\s+/m, ' ').strip.split(" ")[-1]
end

def ipaInitials(name)
  ipaFirstName(name)[0].upcase + ipaLastName(name)[0].upcase
end

